﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Collections;

public class EyeTrackerSupport : MonoBehaviour
{
    [SerializeField]
    private Text message;

    void OnEnable()
    {
        ARFaceManager faceManager = FindObjectOfType<ARFaceManager>();
        if (faceManager != null && faceManager.subsystem != null && faceManager.subsystem.SubsystemDescriptor.supportsEyeTracking)
        {
            message.text = "Eye Tracking is supported on this device";
        }
        else
        {
            message.text = "Eye Tracking is not supported on this device";
            StartCoroutine(LoadOpenCV());
        }
    }

    IEnumerator LoadOpenCV()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("OpenCVSupport");
    }
}